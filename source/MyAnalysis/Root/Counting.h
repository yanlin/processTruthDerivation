#include <string>
#include <vector>
#include <math.h>
#include <iostream>

using namespace std;

#include "xAODMuon/Muon.h"
#include "xAODEgamma/Electron.h"
#include "MyAnalysis/AnalysisVar.h"

void SetWeight(MapType2_Double& map, string cut, string chn, double value) {

    if(chn=="All" || chn=="ALL" || chn=="all") {
      MapType2_Double::iterator it;
      for(it=map.begin(); it!=map.end(); it++) {
        string chn1=(*it).first;
        map[chn1][cut] *= value;
      }
    }
    else {
       map[chn][cut] *= value;
    }
}


void SetFlag(MapType2_Int& map, string cut, string chn, int value) {

    if(chn=="All" || chn=="ALL" || chn=="all") {
      MapType2_Int::iterator it;
      for(it=map.begin(); it!=map.end(); it++) {
        string chn1=(*it).first;
        map[chn1][cut]=value;
      }
    }
    else {
       map[chn][cut]=value;
    }
}


void DoCounting(string sysname, MapType3_Counting& CNT, string chn, string cut, double w=1.0) {
    double num = w*1.0;
    CNT[sysname][chn][cut].num += num;
    CNT[sysname][chn][cut].err = sqrt(pow(CNT[sysname][chn][cut].err,2)+num);
}


void CountMuObj(xAOD::Muon* muon, MapType_VString STEP_obj, MapType3_Counting& CNT, string sysname, bool& passAll) {

    vector<string> objstr = STEP_obj["mu"];
    
    for(int i=0; i<(int)objstr.size(); i++) {
      if(objstr[i].find("OverLap") != string::npos) break;
      passAll = passAll && muon->auxdata< char >( objstr[i].c_str() );
      if(passAll) DoCounting(sysname, CNT, "mu", objstr[i]); 
    }
 
//    muon->auxdata< int >( "mu_Z0" ) = 0;
}

void CountEleObj(xAOD::Electron* electron, MapType_VString STEP_obj, MapType3_Counting& CNT, string sysname, bool& passAll) {

    vector<string> objstr = STEP_obj["ele"];

    for(int i=0; i<(int)objstr.size(); i++) {
      if(objstr[i].find("OverLap") != string::npos) break;
      passAll = passAll && electron->auxdata< char >( objstr[i].c_str() ) ;
      if(passAll) DoCounting(sysname, CNT, "ele", objstr[i]);
    }
}

void CountJetObj(xAOD::Jet* jet, MapType_VString STEP_obj, MapType3_Counting& CNT, string sysname, bool& passAll) {

    vector<string> objstr = STEP_obj["jet"];
    for(int i=0; i<(int)objstr.size(); i++) {
      if(objstr[i].find("OverLap") != string::npos) break;
      passAll = passAll && jet->auxdata< char >( objstr[i].c_str() );
      if(passAll) DoCounting(sysname, CNT, "jet", objstr[i]);
    }
}

void CountJetClean(xAOD::Jet* jet, MapType_VString STEP_obj, MapType3_Counting& CNT, string sysname, bool& passAll) {

    vector<string> objstr = STEP_obj["jet"];
    for(int i=0; i<(int)objstr.size(); i++) {
      if(objstr[i].find("Clean") != string::npos) break;
      passAll = passAll && jet->auxdata< char >( objstr[i].c_str() );
      //if(passAll) DoCounting(sysname, CNT, "jet", objstr[i]);
    }
}




void CountEvt(string sysname, vector<string> CHN, vector<string> STEP_cut, MapType2_Int& FLAG_cut_temp, MapType2_Int& FLAG_cut, MapType3_Counting& CNT, MapType2_Double& Evt_Weight) {

    MapType_VString::iterator it;
    bool passAll;
    for(int i=0; i<(int)CHN.size(); i++) {
      string chl =CHN[i];
      passAll=true;
      for(int j=0; j<(int)STEP_cut.size(); j++) {
        string cut=STEP_cut[j];
        passAll = passAll && FLAG_cut_temp[chl][cut];
        if(passAll) {
          FLAG_cut[chl][cut]=1;
          double w = Evt_Weight[chl][cut];
          DoCounting(sysname, CNT, chl, cut, w);
        }
      }
    }
}






