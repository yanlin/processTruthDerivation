#include <iostream>
#include <fstream>
using namespace std;

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include "xAODMuon/Muon.h"
#include "xAODEventInfo/EventInfo.h"

#include "MyAnalysis/MyxAODAnalysis.h"
#include "MyAnalysis/AnalysisVar.h"

EL::StatusCode MyxAODAnalysis :: initialize ()
{

  
  time(&start);

  SETNAME.push_back("physics");
  InitSetting(SETTING,"physics","docorr=1,dosys=0");

  //InitStrVec(CHN,"mmmm,eeee,eemm");
  InitStrVec(CHN,"mm");
  InitStrVec(STEP_cut,"xAOD,MuonPt,MuonEta,LeadPt,MET");

  //Fill in histograms & trees
  InitHistVar("bornmass",2000,0,200,"xAOD,MuonPt,MuonEta,LeadPt,MET");
  InitHistVar("truthmass",50,0,5000,"xAOD,MuonPt,MuonEta,LeadPt,MET");  
  InitHistVar("met",40,0,200,"xAOD,MuonPt,MuonEta,LeadPt,MET");
  InitHistVar("Zpt",40,0,2000,"xAOD,MuonPt,MuonEta,LeadPt,MET");
  InitHistVar("ZRap,deltaPhi",100,-5,5,"xAOD,MuonPt,MuonEta,LeadPt,MET");
  InitHistVar("bdt_output,CosThetaStar,cosTS",100,-1,1,"MET");
  InitHistVar("njet",20,0,20,"MET");
  InitHistVar("nbjet",20,0,20,"MET");
  InitHistVar("njetOR",20,0,20,"MET");
  InitHistVar("jet_eta1,jet_eta2,jet_phi1,jet_phi2",100,-5,5,"MET");
  InitHistVar("jet_pt1,jet_pt2",2000,0,10000,"MET");
  InitHistVar("m2j,Pt_jj,Ht_total,Pt_llj1,Pt_llj2,Pt_lljj",2000,0,10000,"MET");
  InitHistVar("etaproduct_jj",400,-20,20,"MET");
  InitHistVar("deltaY_llj1,deltaY_llj2,deltaY_lljj,deltaRjj,deltaEtajj,deltaPhijj,centrality",100,0,10,"MET");
  //0) Event Info
  InitHistVar("Run,Event", 2,0,2,"xAOD");
  //1) Pass Cut or not
  InitHistVar("PassxAOD,PassMuonPt,PassMuonEta,PassLeadPt,PassMET",2,0,2,"xAOD");
  //2) Event weights
  InitHistVar("MCEventWeight",100,0,10,"xAOD");
  //3) Truth and Reco muon four momentum
  InitHistVar("LeadEta,SubEta,LeadPhi,SubPhi",100,-5,5,"MET");
  InitHistVar("deltaEta,deltaR",100,0,10,"MET");
  InitHistVar("LeadPt,SubPt",40,0,2000,"MET");
 
  m_event = wk()->xaodEvent();
  Info("initialize()", "Number of events = %lli", m_event->getEntries() );

  /*
  fsrTool = new FSR::FsrPhotonTool("FsrPhotonTool");
  fsrTool->setProperty<double>("drcut", 0.1);
  if ( !fsrTool->initialize().isSuccess() ){
      Error("initialize()", "Failed to properly initialize the FSRPhotonTool Tool. Exiting." );
      return EL::StatusCode::FAILURE;
  }
  */
 
  const xAOD::EventInfo* eventInfo = 0;
  if( !m_event->retrieve( eventInfo, "EventInfo").isSuccess() )
  {
    Error("execute ()", "Failed to retrieve EventInfo. Exiting." );
    return EL::StatusCode::FAILURE;
  }
  isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );

  SYSNAME.push_back("NOMINAL");

  CreateCountingMap();

  h_truthmass = new TH1F("h_truthmass", "h_truthmass", 200, 0, 200);

  h_random = new TH1F("h_random", "h_random", 100, -0.5, 0.5);

  //double ptbins[14] = {5., 20., 25., 30., 35., 40., 45., 50., 60., 70., 80., 90., 100., 150.};
  double etabins[53], dptbins[121];

  double ptbins[30];
  ptbins[0] = 5.;
  ptbins[1] = 20.;
  ptbins[2] = 25.;
  ptbins[3] = 30.;
  
  for(int ptindex=0; ptindex<=19; ptindex++)
    ptbins[4+ptindex] = 31+ptindex; 
  
  ptbins[24] = 60.;
  ptbins[25] = 70.;
  ptbins[26] = 80.;
  ptbins[27] = 90.;
  ptbins[28] = 100.;
  ptbins[29] = 150.;

  for(int index=0; index<=52; index++)
    etabins[index] = -2.6+index*0.1;

  //histo_nmu_pt_eta_truth = new TH2F("histo_nmu_pt_eta_truth", "histo_nmu_pt_eta_truth",  13, ptbins, 26, etabins);

  histo_nmu_pt_eta_truth = new TH2F("histo_nmu_pt_eta_truth", "histo_nmu_pt_eta_truth",  29, ptbins, 52, etabins);

  TFile *file1 = wk()->getOutputFile ("tree_output");
  TFile *file2 = wk()->getOutputFile ("hist_output");

  h_truthmass->SetDirectory (file2);
  h_random->SetDirectory (file2);
  histo_nmu_pt_eta_truth->SetDirectory (file2);

  CreateTreeVarHistoMap(file2);
  
  // add output tree to store dimuon information
  TTree *tree_incl = new TTree("tree_incl", "output tree"); //!
  tree_incl->SetDirectory(file1);
  MapType2_Double::iterator it;
  for(it=HistVar.begin(); it!=HistVar.end(); it++) {
      string varname=(*it).first;
      tree_incl->Branch(varname.c_str(),&HistVar[varname]["Value"]);
  }
  tree_incl->Print();  
  

  m_eventCounter = 0;
  m_filter = 0;
  m_fidu1 = 0;
  m_fidu2 = 0;
  m_fidu3 = 0;


  return EL::StatusCode::SUCCESS;

}


void MyxAODAnalysis :: ClearFlags(MapType2_Int& map) {

    MapType2_Int::iterator it;
    for(it=map.begin(); it!=map.end(); it++) {
        string chn=(*it).first;
        MapType_Int::iterator it2;
        for(it2=(*it).second.begin(); it2!=(*it).second.end(); it2++) {
            string cut=(*it2).first;
            map[chn][cut] = 0;
        }
    }
}

void MyxAODAnalysis :: ClearWeight(MapType2_Double& map) {
    MapType2_Double::iterator it;

    MapType_Double::iterator iit;

    for(it=map.begin(); it!=map.end(); it++)
      for(iit=(*it).second.begin(); iit!=(*it).second.end(); iit ++)
        map[(*it).first][(*iit).first]=1.0;
}



void MyxAODAnalysis :: CreateCountingMap() {

    MapType_VString::iterator it;
    for(it=STEP_obj.begin(); it!=STEP_obj.end(); it++) {
      string obj=(*it).first;
      for(int i=0; i<(int)(*it).second.size(); i++) {
        string cut=STEP_obj[obj][i];
        COUNT ini={0.,0.};

        for(int j=0; j<(int)SYSNAME.size(); j++) {
          CNT_obj[SYSNAME[j]][obj][cut]=ini;
        }

      }
    }

    for(int i=0; i<(int)CHN.size(); i++) {
      string chn=CHN[i];
      for(int j=0; j<(int)STEP_cut.size(); j++) { 
        string cut=STEP_cut[j];
        FLAG_cut_temp[chn][cut]=0;
        FLAG_cut[chn][cut]=0;
        COUNT ini={0.,0.};

        for(int k=0; k<(int)SYSNAME.size(); k++) { 
          CNT_cut[SYSNAME[k]][chn][cut]=ini;
        }
      }
    }

    for(int j=0; j<(int)STEP_cut.size(); j++) {
      string cut=STEP_cut[j];
      MapType_Double weight;
      weight["evt_weight_reco"] = 1.0;
      Evt_Weight[cut]=weight;
    }

}


void MyxAODAnalysis :: InitStrVec(vector<string>& out, string in, string de) {
    int pos=0, pos_pre=0;
    while(true) {
        pos=in.find(de,pos_pre);
        if(pos==-1) {out.push_back(in.substr(pos_pre,in.size()-pos_pre)); break;}
        else  out.push_back(in.substr(pos_pre,pos-pos_pre));
        pos_pre=pos+1;
    }
}



void MyxAODAnalysis :: InitObjSTEP(MapType_VString& STEP, string obj, string steps) {
    vector<string> str, objstr;
    InitStrVec(str, steps, ",");

    for(int i=0; i<(int)str.size(); i++) {
      string objs = str[i];
      objstr.push_back(objs);
    }

    STEP[obj]=objstr;
}

void MyxAODAnalysis :: ClearVariables(MapType2_Int& map) {

    MapType2_Int::iterator it;
    for(it=map.begin(); it!=map.end(); it++) {
      string varname = (*it).first;
      map[varname]["Value"]=-9999;
    }
}

void MyxAODAnalysis :: ClearVariables(MapType2_Float& map) {

    MapType2_Float::iterator it;
    for(it=map.begin(); it!=map.end(); it++) {
      string varname = (*it).first;
      map[varname]["Value"]=-9999.;
    }
}



void MyxAODAnalysis :: ClearVariables(MapType2_Double& map) {

    MapType2_Double::iterator it;
    for(it=map.begin(); it!=map.end(); it++) {
      string varname = (*it).first;
      map[varname]["Value"]=-9999.;
    }
}

void MyxAODAnalysis :: ClearVariables(MapType2_Double2D& map) {

    MapType2_Double2D::iterator it;
    pair<double, double> init (-9999.0, -9999.0);
    for(it=map.begin(); it!=map.end(); it++) {
      string varname = (*it).first;
      map[varname]["Value"] = init;
    }
}

void MyxAODAnalysis :: ClearVariables(MapType2_VDouble& map) {

    MapType2_VDouble::iterator it;
    for(it=map.begin(); it!=map.end(); it++) {
      string varname = (*it).first;
      map[varname]["Value"].clear();
      map[varname]["NBins"].clear();
      map[varname]["Min"].clear();
      map[varname]["Max"].clear();
      map[varname]["Weight"].clear();
    }
}

void MyxAODAnalysis :: ClearVariables(MapType2_V2DDouble& map) {

    MapType2_V2DDouble::iterator it;
    for(it=map.begin(); it!=map.end(); it++) {
      string varname = (*it).first;
      map[varname]["Value"].clear();
      map[varname]["NBins"].clear();
      map[varname]["Min"].clear();
      map[varname]["Max"].clear();
      map[varname]["Weight"].clear();
    }
}



void MyxAODAnalysis :: InitTreeVar(string varlist, string type) {
    vector<string> variables;
    InitStrVec(variables, varlist, ",");

    if(type=="I") {
      for(int i=0; i<(int)variables.size(); i++) {
        TreeIntVar[variables[i]]["Value"]=-9999;
      }
    }

    if(type=="F") {
      for(int i=0; i<(int)variables.size(); i++) {
        TreeFltVar[variables[i]]["Value"]=-9999.0;
      }
    }

}

void MyxAODAnalysis :: InitHistVar(string varlist, int nbin, double xmin, double xmax, string cutstep) {
    vector<string> variables;
    InitStrVec(variables, varlist, ",");

    vector<string> cuts;
    if(cutstep=="") cuts.clear();
    else if(cutstep=="All") cuts = STEP_cut;
    else if(cutstep.compare(0,1,"-")==0) {
      cutstep = cutstep.erase(0,1);
      for(int i=0; i<(int)STEP_cut.size(); i++) {
        if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
        else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
      }
    }else {
      InitStrVec(cuts, cutstep, ",");
    }

    for(int i=0; i<(int)variables.size(); i++) {
     
      HistVar[variables[i]]["Value"]=-9999.0;
      HistVar[variables[i]]["NBins"]=nbin;
      HistVar[variables[i]]["Xmin"]=xmin;
      HistVar[variables[i]]["Xmax"]=xmax;
      HistVar[variables[i]]["Weight"]=1.0;
   
      for(int j=0; j<(int)cuts.size(); j++) {
        HistVar[variables[i]][cuts[j]]=1;
      }
    }

}

void MyxAODAnalysis :: InitHistVar(string varlist, int nbin, double xmin, double xmax, int PDFnum, string cutstep) {
    vector<string> variables;
    InitStrVec(variables, varlist, ",");

    vector<string> cuts;
    if(cutstep=="") cuts.clear();
    else if(cutstep=="All") cuts = STEP_cut;
    else if(cutstep.compare(0,1,"-")==0) {
      cutstep = cutstep.erase(0,1);
      for(int i=0; i<(int)STEP_cut.size(); i++) {
        if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
        else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
      }
    }else {
      InitStrVec(cuts, cutstep, ",");
    }

    for(int i=0; i<(int)variables.size(); i++) {
    
      for(int j=1; j<=PDFnum; j++) {
        string num;
        stringstream ss;
        ss << j;
        num=ss.str();

        string varname = variables[i]+"_"+num;

        HistVar[varname]["Value"]=-9999.0;
        HistVar[varname]["NBins"]=nbin;
        HistVar[varname]["Xmin"]=xmin;
        HistVar[varname]["Xmax"]=xmax;
        HistVar[varname]["Weight"]=1.0;

        for(int j=0; j<(int)cuts.size(); j++) {
          HistVar[varname][cuts[j]]=1;
        }
      }
    }

}

void MyxAODAnalysis :: InitHist2DVar(string varlist, int nxbin, double xmin, double xmax,
                                     int nybin, double ymin, double ymax, string cutstep) {
    vector<string> variables;
    InitStrVec(variables, varlist, ",");

    vector<string> cuts;
    if(cutstep=="") cuts.clear();
    else if(cutstep=="All") cuts = STEP_cut;
    else if(cutstep.compare(0,1,"-")==0) {
      cutstep = cutstep.erase(0,1);
      for(int i=0; i<(int)STEP_cut.size(); i++) {
        if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
        else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
      }
    }else {
      InitStrVec(cuts, cutstep, ",");
    }

    pair<double, double> init (-9999.0, -9999.0);
    for(int i=0; i<(int)variables.size(); i++) {

      Hist2DVar[variables[i]]["Value"]=init;
      Hist2DVar[variables[i]]["NBins"]=make_pair(nxbin, nybin);
      Hist2DVar[variables[i]]["Min"]=make_pair(xmin, ymin);
      Hist2DVar[variables[i]]["Max"]=make_pair(xmax, ymax);

      for(int j=0; j<(int)cuts.size(); j++) {
        Hist2DVar[variables[i]][cuts[j]]=make_pair(1,1);
      }
    }
}


void MyxAODAnalysis :: InitVVar(string varlist, int nbin, double xmin, double xmax, string cutstep) {
    vector<string> variables;
    InitStrVec(variables, varlist, ",");

    vector<string> cuts;
    if(cutstep=="") cuts.clear();
    else if(cutstep=="All") cuts = STEP_cut;
    else if(cutstep.compare(0,1,"-")==0) {
      cutstep = cutstep.erase(0,1);
      for(int i=0; i<(int)STEP_cut.size(); i++) {
        if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
        else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
      }
    }else {
      InitStrVec(cuts, cutstep, ",");
    }

    for(int i=0; i<(int)variables.size(); i++) {
    
      VVar[variables[i]]["Value"].clear();
      VVar[variables[i]]["NBins"].push_back(nbin);
      VVar[variables[i]]["Xmin"].push_back(xmin);
      VVar[variables[i]]["Xmax"].push_back(xmax);
      VVar[variables[i]]["Weight"].clear();
  
      for(int j=0; j<(int)cuts.size(); j++) {
        VVar[variables[i]][cuts[j]].push_back(1);
      }
    }

}

void MyxAODAnalysis :: InitHist2DVVar(string varlist, int nxbin, double xmin, double xmax,int nybin, double ymin, double ymax, string cutstep) {
    vector<string> variables;
    InitStrVec(variables, varlist, ",");

    vector<string> cuts;
    if(cutstep=="") cuts.clear();
    else if(cutstep=="All") cuts = STEP_cut;
    else if(cutstep.compare(0,1,"-")==0) {
      cutstep = cutstep.erase(0,1);
      for(int i=0; i<(int)STEP_cut.size(); i++) {
        if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
        else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
      }
    }else {
      InitStrVec(cuts, cutstep, ",");
    }

    for(int i=0; i<(int)variables.size(); i++) {

      V2DVar[variables[i]]["Value"].clear();
      V2DVar[variables[i]]["NBins"].push_back(make_pair(nxbin, nybin));
      V2DVar[variables[i]]["Min"].push_back(make_pair(xmin, ymin));
      V2DVar[variables[i]]["Max"].push_back(make_pair(xmax, ymax));

      for(int j=0; j<(int)cuts.size(); j++) {
        V2DVar[variables[i]][cuts[j]].push_back(make_pair(1,1));
      }
    }
}



void MyxAODAnalysis :: AddVarIntoTree(TTree *tree) {

    char buf[1000];
    getcwd(buf, sizeof(buf));
    setenv("LOCAL", buf, 1);
    FILE* fp;
    char result_buf[1000];
    fp = popen("find $LOCAL -name MiniTree.txt", "r");
    fgets(result_buf, sizeof(result_buf), fp);

    string varfile(result_buf);
    size_t len = varfile.size();
    varfile.erase(len-1);
    ifstream file;
    file.open(varfile.c_str(), ios::out);

    if (file.is_open())  {
      char line[256];
      while (!file.eof() )  {
        string varname, type;

        file.getline (line,100);
        string sline(line);

        if(sline.find("Int_t")!=string::npos) {
          type = "I";
          varname = sline.substr(9);
          type = varname+"/"+type;
          InitTreeVar(varname,"I");
          tree->Branch(varname.c_str(),&TreeIntVar[varname]["Value"], type.c_str());
          //tree->Branch(varname.c_str(),&Var[varname]["Value"]);
        }
        if(sline.find("Float_t")!=string::npos) {
          type = "F";
          varname = sline.substr(9);
          type = varname+"/"+type;
          InitTreeVar(varname, "F");
          tree->Branch(varname.c_str(),&TreeFltVar[varname]["Value"], type.c_str());
          //tree->Branch(varname.c_str(),&TreeFltVar[varname]["Value"]);
        }
      }
    }
}

void MyxAODAnalysis :: CreateTreeVarHistoMap(TFile* file) {
    
  for(int k=0; k<(int)SYSNAME.size(); k++) {
    string sysname = SYSNAME[k];

    if(sysname!="NOCORR" && (!SETTING["physics"]["docorr"])) continue;

    if(sysname!="NOMINAL" && SETTING["physics"]["docorr"] && (!SETTING["physics"]["dosys"])) continue;

    if(sysname=="NOCORR" && SETTING["physics"]["docorr"] && SETTING["physics"]["dosys"]) continue;

    MapType2_Double::iterator it;
    for(it=HistVar.begin(); it!=HistVar.end(); it++) {
      string varname=(*it).first;
      int nbin = int((*it).second["NBins"]);
      double xlow = double((*it).second["Xmin"]);
      double xhigh = double((*it).second["Xmax"]);

      if(nbin == 0) continue;

      for(int i=0; i<(int)CHN.size(); i++) {
        string chn=CHN[i];

        for(int j=0; j<(int)STEP_cut.size(); j++) {
          string cut=STEP_cut[j];
          if(int((*it).second[cut])!=1) continue;

          string histo_name = sysname + "_" + chn + "_" + cut + "_" + varname;
          TH1F *histo_pointer = new TH1F(histo_name.c_str(),histo_name.c_str(),nbin,xlow,xhigh);
          histo[sysname][chn][cut][varname]=histo_pointer;
          histo[sysname][chn][cut][varname]->SetDirectory(file);
        }
      }
    }

    

    MapType2_Double2D::iterator it2D;
    for(it2D=Hist2DVar.begin(); it2D!=Hist2DVar.end(); it2D++) {
      string varname=(*it2D).first;

      int nxbin = int((*it2D).second["NBins"].first);
      double xlow = double((*it2D).second["Min"].first);
      double xhigh = double((*it2D).second["Max"].first);
      int nybin = int((*it2D).second["NBins"].second);
      double ylow = double((*it2D).second["Min"].second);
      double yhigh = double((*it2D).second["Max"].second);

      if(nxbin == 0) continue;

      for(int i=0; i<(int)CHN.size(); i++) {
        string chn=CHN[i];

        for(int j=0; j<(int)STEP_cut.size(); j++) {
          string cut=STEP_cut[j];

          pair<double, double> ncuts = (*it2D).second[cut];
          if(int(ncuts.first)!=1) continue;

          string histo_name = sysname + "_" + chn + "_" + cut + "_" + varname;
          TH2F *histo_pointer = new TH2F(histo_name.c_str(),histo_name.c_str(),nxbin,xlow,xhigh,nybin,ylow,yhigh);
          histo_2D[sysname][chn][cut][varname]=histo_pointer;
          histo_2D[sysname][chn][cut][varname]->SetDirectory(file);
        }
      }
    }


    MapType2_VDouble::iterator itv;
    for(itv=VVar.begin(); itv!=VVar.end(); itv++) {
      string varname=(*itv).first;
      int nbin = int((*itv).second["NBins"][0]);
      double xlow = double((*itv).second["Xmin"][0]);
      double xhigh = double((*itv).second["Xmax"][0]);

      if(nbin == 0) continue;

      for(int i=0; i<(int)CHN.size(); i++) {
        string chn=CHN[i];

        for(int j=0; j<(int)STEP_cut.size(); j++) {
          string cut=STEP_cut[j];
          if(int((*itv).second[cut].size())!=1) continue;
          if(int((*itv).second[cut][0])!=1) continue;

          string histo_name = sysname + "_" + chn + "_" + cut + "_" + varname;
          TH1F *histo_pointer = new TH1F(histo_name.c_str(),histo_name.c_str(),nbin,xlow,xhigh);
          histo[sysname][chn][cut][varname]=histo_pointer;
          histo[sysname][chn][cut][varname]->SetDirectory(file);
        }
      }
    }

    MapType2_V2DDouble::iterator it2Dv;
    for(it2Dv=V2DVar.begin(); it2Dv!=V2DVar.end(); it2Dv++) {
        string varname=(*it2Dv).first;

        int nxbin = int((*it2Dv).second["NBins"][0].first);
        double xlow = double((*it2Dv).second["Min"][0].first);
        double xhigh = double((*it2Dv).second["Max"][0].first);
        int nybin = int((*it2Dv).second["NBins"][0].second);
        double ylow = double((*it2Dv).second["Min"][0].second);
        double yhigh = double((*it2Dv).second["Max"][0].second);

        if(nxbin == 0) continue;

        for(int i=0; i<(int)CHN.size(); i++) {
          string chn=CHN[i];

          for(int j=0; j<(int)STEP_cut.size(); j++) {
            string cut=STEP_cut[j];

            if(int((*it2Dv).second[cut].size())!=1) continue;
            pair<double, double> ncuts = (*it2Dv).second[cut][0];
            if(int(ncuts.first)!=1) continue;

            string histo_name = sysname + "_" + chn + "_" + cut + "_" + varname;
            TH2F *histo_pointer = new TH2F(histo_name.c_str(),histo_name.c_str(),nxbin,xlow,xhigh,nybin,ylow,yhigh);
            histo_v2D[sysname][chn][cut][varname]=histo_pointer;
            histo_v2D[sysname][chn][cut][varname]->SetDirectory(file);
          }
        }
      }


  }
    
}

void MyxAODAnalysis :: FillHistograms(string sysname) {

  MapType2_Double::iterator it;
  for(it=HistVar.begin(); it!=HistVar.end(); it++) {
    string varname=(*it).first;

    if((*it).second["NBins"] == 0) continue;

    for(int i=0; i<(int)CHN.size(); i++) {
      string chn=CHN[i];

      for(int j=0; j<(int)STEP_cut.size(); j++) {
        string cut=STEP_cut[j];
        if(int((*it).second[cut])==1 && FLAG_cut[chn][cut])
          //cout<<"Evt_Weight: "<<Evt_Weight["All"]["weight"]<<endl; 
          //histo[sysname][chn][cut][varname]->Fill(HistVar[varname]["Value"], HistVar[varname]["Weight"]);
          histo[sysname][chn][cut][varname]->Fill(HistVar[varname]["Value"], Evt_Weight["All"]["weight"]);
      } 
    }
  } 

  MapType2_Double2D::iterator it2D;
  for(it2D=Hist2DVar.begin(); it2D!=Hist2DVar.end(); it2D++) {
    string varname=(*it2D).first;

    pair<double, double> nbins = (*it2D).second["NBins"];
    if(nbins.first == 0) continue;

    for(int i=0; i<(int)CHN.size(); i++) {
      string chn=CHN[i];

      for(int j=0; j<(int)STEP_cut.size(); j++) {
        string cut=STEP_cut[j];
        pair<double, double> ncuts = (*it2D).second[cut];
        if(int(ncuts.first)==1 && FLAG_cut[chn][cut]) {
          pair<double, double> values = (*it2D).second["Value"];
          histo_2D[sysname][chn][cut][varname]->Fill(values.first, values.second);
        }
      }
    }
  }


  MapType2_VDouble::iterator itv;
  for(itv=VVar.begin(); itv!=VVar.end(); itv++) {
    string varname=(*itv).first;

    if((*itv).second["NBins"][0] == 0) continue;

    for(int i=0; i<(int)CHN.size(); i++) {
      string chn=CHN[i];

      for(int j=0; j<(int)STEP_cut.size(); j++) {
        string cut=STEP_cut[j];
        if(int((*itv).second[cut].size())!=1) continue;
        if(int((*itv).second[cut][0])==1 && FLAG_cut[chn][cut]) {
          for(int k=0; k<(int)VVar[varname]["Value"].size(); k++)
            if(VVar[varname]["Weight"].size()>0)
              histo[sysname][chn][cut][varname]->Fill(VVar[varname]["Value"][k], VVar[varname]["Weight"][k]);
            else histo[sysname][chn][cut][varname]->Fill(VVar[varname]["Value"][k]);
        }
      }
    }
  }

  MapType2_V2DDouble::iterator it2Dv;
  for(it2Dv=V2DVar.begin(); it2Dv!=V2DVar.end(); it2Dv++) {
    string varname=(*it2Dv).first;

    pair<double, double> nbins = (*it2Dv).second["NBins"][0];
    if(nbins.first == 0) continue;

    for(int i=0; i<(int)CHN.size(); i++) {
      string chn=CHN[i];

      for(int j=0; j<(int)STEP_cut.size(); j++) {
        string cut=STEP_cut[j];

        if(int((*it2Dv).second[cut].size())!=1) continue;

        if(int((*it2Dv).second[cut][0].first)==1 && FLAG_cut[chn][cut]) {
          for(int k=0; k<(int)V2DVar[varname]["Value"].size(); k++) {
            pair<double, double> values = V2DVar[varname]["Value"][k];
            if(V2DVar[varname]["Weight"].size()>0)
              histo_v2D[sysname][chn][cut][varname]->Fill(values.first, values.second, V2DVar[varname]["Weight"][k].first);
            else histo_v2D[sysname][chn][cut][varname]->Fill(values.first, values.second);
          }
        }
      }
    }
  }

}


void MyxAODAnalysis :: InitSetting(MapType2_Int& setmap, string setname, string settings) {
    vector<string> vsettings;
    InitStrVec(vsettings, settings, ",");
    for(int i=0; i<(int)vsettings.size(); i++) {
        vector<string> pairs;
        InitStrVec(pairs,vsettings[i],"=");
        if(pairs.size()<2) {
            cout<<"Error in setting: can not parse "<<vsettings[i]<<endl;
            exit(-1);
        }
        setmap[setname][pairs[0]]=atoi(pairs[1].c_str());
    }
}

double MyxAODAnalysis :: calcCosThetaStar(const TLorentzVector &e, const TLorentzVector &p) {
  TLorentzVector ep = e+p;
  Double_t dir = ep.Pz()/TMath::Abs(ep.Pz());
  Double_t num = ((e.E()+e.Pz())*(p.E()-p.Pz()))-((e.E()-e.Pz())*(p.E()+p.Pz()));
  Double_t den = (ep.M())*TMath::Sqrt((ep.M())*(ep.M())+(ep.Pt())*(ep.Pt()));
  Double_t cts = dir*(num/den);
  return cts;
}
