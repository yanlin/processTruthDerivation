#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoopAlgs/AlgSelect.h>

#include "MyAnalysis/MyxAODAnalysis.h"
#include "MyAnalysis/OBJ_Base.h"

// Book Keeper
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODJet/JetContainer.h"
#include <xAODMuon/Muon.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/MuonAuxContainer.h>
#include "xAODEgamma/ElectronxAODHelpers.h"
#include "xAODEventInfo/EventInfo.h"
#include <xAODMissingET/MissingETContainer.h>

#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

#include "Initialize.h"
#include "Counting.h"


using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)



MyxAODAnalysis :: MyxAODAnalysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  //m_setSysList=new std::vector<std::string>();
}

MyxAODAnalysis :: MyxAODAnalysis (string treename="physics")
{

  set = treename;
  //m_setSysList=new std::vector<std::string>();

}



EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  EL::OutputStream output1("tree_output");
  job.outputAdd (output1);

  EL::OutputStream output2("hist_output");
  job.outputAdd (output2);

  EL::OutputStream output3("cutflow");
  job.outputAdd (output3); 

  job.useXAOD ();

  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init( "MyxAODAnalysis" ).ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  TH1::SetDefaultSumw2();
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}

// < move the "initialize ()" to a separate file "initialize.h"


EL::StatusCode MyxAODAnalysis :: execute ()
{

    // clean up list and dictionary
    ClearFlags(FLAG_cut_temp);
    ClearFlags(FLAG_cut);
    ClearWeight(Evt_Weight);
    ClearVariables(HistVar);
    ClearVariables(TreeIntVar);
    ClearVariables(TreeFltVar);

    //retrieve basic event information
    m_eventCounter++;
    if(m_eventCounter==1 || m_eventCounter%1000==0)
        cout << "event counter " << m_eventCounter << endl;

    const xAOD::EventInfo* eventInfo = 0;
    if( !m_event->retrieve( eventInfo, "EventInfo").isSuccess() )
    {
        Error("execute ()", "Failed to retrieve EventInfo. Exiting." );
        return EL::StatusCode::FAILURE;
    }
  
    uint32_t run;
    unsigned long long event;
    if(isMC) {
        run = eventInfo->mcChannelNumber();
        event = eventInfo->mcEventNumber();
    }else {
        run = eventInfo->runNumber();
        event = eventInfo->eventNumber();
    }

    //cout<<"event number: "<<event<<endl;

    float gen_weight = 1.;
    vector<float> vw = eventInfo->mcEventWeights();
    gen_weight = eventInfo->mcEventWeight();

    //cout<<"gen_weight: "<<gen_weight<<endl;

    // Truth Particle Container Retrieval
    /*
    const xAOD::TruthParticleContainer* mc_particle = 0;
        if (isMC){
            if ( ! m_event->retrieve(mc_particle,"TruthParticles").isSuccess() ){
                Error("execute()", "Failed to retrieve TruthParticleContainer container. Exiting." );
            return EL::StatusCode::FAILURE;
        }
    }    
    */

    const xAOD::TruthParticleContainer* TruthMuons = 0;
        if (isMC){
            if ( ! m_event->retrieve(TruthMuons, "TruthMuons").isSuccess() ){
                Error("execute()", "Failed to retrieve TruthMuons container. Exiting." );
            return EL::StatusCode::FAILURE;
        }
    }

    const xAOD::TruthParticleContainer* TruthElectrons = 0;
        if (isMC){
            if ( ! m_event->retrieve(TruthElectrons, "TruthElectrons").isSuccess() ){
                Error("execute()", "Failed to retrieve TruthElectrons container. Exiting." );
            return EL::StatusCode::FAILURE;
        }
    }

    bool ISmuchan;
    //cout<<"Arg_SampleName: "<<Arg_SampleName<<endl;
    if (Arg_SampleName != "ee") ISmuchan = true;
    else ISmuchan = false;

    vector<TLorentzVector> goodlep; 
    vector<int> lepcharge;

    if (ISmuchan){
      for(auto truth = TruthMuons->begin(); truth!=TruthMuons->end(); ++truth) {

            float pt = (*truth)->auxdata<float>("pt_dressed"); 
            float eta = (*truth)->auxdata<float>("eta_dressed");
            float phi = (*truth)->auxdata<float>("phi_dressed");
            float energy = (*truth)->auxdata<float>("e_dressed");

            TLorentzVector tmp_lep;
            tmp_lep.SetPtEtaPhiE(pt, eta, phi, energy);
            goodlep.push_back(tmp_lep);
            lepcharge.push_back((*truth)->charge());


            //cout<<"truth muon pt: "<<(*truth)->pt()*0.001<<" eta: "<<(*truth)->eta()<<endl;
            //cout<<"dressed pt: "<<(*truth)->pt_dressed()*0.001<<" eta: "<<(*truth)->eta_dressed()<<endl;
            //cout<<"dressed pt: "<<(*truth)->auxdata<float>("pt_dressed")*0.001<<" eta: "<<(*truth)->auxdata<float>("eta_dressed")<<endl;
      }
    }

    if (!ISmuchan){
      for(auto truth = TruthElectrons->begin(); truth!=TruthElectrons->end(); ++truth) {

            float pt = (*truth)->auxdata<float>("pt_dressed");
            float eta = (*truth)->auxdata<float>("eta_dressed");
            float phi = (*truth)->auxdata<float>("phi_dressed");
            float energy = (*truth)->auxdata<float>("e_dressed");

            TLorentzVector tmp_lep;
            tmp_lep.SetPtEtaPhiE(pt, eta, phi, energy);
            goodlep.push_back(tmp_lep);
            lepcharge.push_back((*truth)->charge());

            //cout<<"electron pt: "<<pt*0.001<<" charge: "<<(*truth)->charge()<<endl;

            //cout<<"truth ele pt: "<<(*truth)->pt()*0.001<<" eta: "<<(*truth)->eta()<<" charge: "<<(*truth)->charge()<<endl;
            //cout<<"dressed pt: "<<(*truth)->auxdata<float>("pt_dressed")*0.001<<" eta: "<<(*truth)->auxdata<float>("eta_dressed")<<endl;
      }
    }
 
    TLorentzVector leadlep, sublep;
    TLorentzVector tmp_lep;
    int tmp_charge;
    for (int i=0; i<(int)goodlep.size()-1; i++)
        for (int j=i+1; j<(int)goodlep.size();j++){
          if(goodlep[i].Pt()<goodlep[j].Pt()){
             tmp_lep = goodlep[i];
             goodlep[i] = goodlep[j];
             goodlep[j] = tmp_lep;

             tmp_charge = lepcharge[i];
             lepcharge[i] = lepcharge[j];
             lepcharge[j] = tmp_charge;  
        }
    }

    leadlep = goodlep[0];
    sublep = goodlep[1];

    //cout<<"lead pt: "<<leadlep.Pt()*0.001<<" sublead pt: "<<sublep.Pt()*0.001<<endl;
    //cout<<"lead charge: "<<lepcharge[0]<<" sub charge: "<<lepcharge[1]<<endl;


    int nlep_truth = int(goodlep.size());

    //if(nlep_truth != 2) cout<<"nlep_truth: "<<nlep_truth<<endl;

    int OS = lepcharge[0]*lepcharge[1];
    if (OS != -1) return EL::StatusCode::SUCCESS; 
  
    Double_t cosTS;
    if(lepcharge[0]==1 && lepcharge[1]==-1) cosTS = calcCosThetaStar(goodlep[1],goodlep[0]);
    if(lepcharge[0]==-1 && lepcharge[1]==1) cosTS = calcCosThetaStar(goodlep[0],goodlep[1]);

    // retrieve truth MET
    TLorentzVector p4met;
    const xAOD::MissingETContainer* mets_truth = 0;
    if ( !m_event->retrieve( mets_truth, "MET_Truth" ).isSuccess() ) {
      Error("execute()", "Failed to retrieve MissingET Truth container. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    const xAOD::MissingET* met_NoInt = (*mets_truth)["NonInt"];
    const xAOD::MissingET* met_Int = (*mets_truth)["Int"];
    const xAOD::MissingET* met_IntOut = (*mets_truth)["IntOut"];
    const xAOD::MissingET* met_IntMuons = (*mets_truth)["IntMuons"];

    float met = met_NoInt->met();
    float mpx = met_NoInt->mpx();
    float mpy = met_NoInt->mpy();
      
    p4met.SetPxPyPzE(mpx, mpy, 0, met);
    //cout<<"met: "<<met<<" p4met E: "<<p4met.E()<<endl;

    //retrieve truth jets
    vector<TLorentzVector> goodj;
    const xAOD::JetContainer* TruthJets = 0;
    //if( !m_event->retrieve( TruthJets, "AntiKt4TruthJets").isSuccess() ){
    if( !m_event->retrieve( TruthJets, "AntiKt4TruthDressedWZJets").isSuccess() ){
      Error("excute()", "Failed to retrieve Truth Jets info. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    xAOD::JetContainer::const_iterator trJets_itr = TruthJets->begin();
    xAOD::JetContainer::const_iterator trJets_end = TruthJets->end(); 
    for( ; trJets_itr != trJets_end; ++trJets_itr ) {
      if(((*trJets_itr)->pt()>25.e3 && fabs((*trJets_itr)->eta())<2.4) || ((*trJets_itr)->pt()>30.e3 && fabs((*trJets_itr)->eta())<4.5)) {
          TLorentzVector tempj;
          tempj.SetPxPyPzE((*trJets_itr)->px()/1000., (*trJets_itr)->py()/1000., (*trJets_itr)->pz()/1000., (*trJets_itr)->e()/1000.);
          goodj.push_back(tempj);
      }
    }


    SetFlag(FLAG_cut_temp,"xAOD","All", 1);

    TLorentzVector tmp_jet;
 
    for (int i=0; i<(int)goodj.size()-1; i++)
        for (int j=i+1; j<(int)goodj.size();j++){
          if(goodj[i].Pt()<goodj[j].Pt()){
             tmp_jet = goodj[i];
             goodj[i] = goodj[j];
             goodj[j] = tmp_jet;
          }
    }

    TLorentzVector Propagator = leadlep + sublep;

    float p_z_ll = Propagator.Pt()*0.001 * TMath::SinH(Propagator.Eta());
    float e_1 = leadlep.Pt()*0.001 * TMath::CosH(leadlep.Eta());
    float e_2 = sublep.Pt()*0.001 * TMath::CosH(sublep.Eta());
    float p_z_1 = leadlep.Pt()*0.001 * TMath::SinH(leadlep.Eta());
    float p_z_2 = sublep.Pt()*0.001 * TMath::SinH(sublep.Eta());
    float p_1_plus = (e_1 + p_z_1) / TMath::Sqrt(2);
    float p_1_minus = (e_1 - p_z_1) / TMath::Sqrt(2);
    float p_2_plus = (e_2 + p_z_2) / TMath::Sqrt(2);
    float p_2_minus = (e_2 - p_z_2) / TMath::Sqrt(2);

    float mass = Propagator.M()*0.001;
    float sqrt_exp = TMath::Sqrt( pow(mass, 2) * (pow(mass, 2) + pow(Propagator.Pt()*0.001, 2)) );
    float sign = p_z_ll / TMath::Abs(p_z_ll);
    double cthstr = 2 * (p_1_plus*p_2_minus - p_1_minus*p_2_plus) / sqrt_exp * sign;

    Double_t mu_ht = 0;
    Double_t jet_ht = 0;
    Double_t total_ht = 0;

    mu_ht = leadlep.Pt() + sublep.Pt();
    for(int i=0; i<(int)goodj.size(); i++){
        jet_ht = jet_ht + goodj[i].Pt();
    }

    total_ht = mu_ht + jet_ht;
    
    //Fill in tree / histogram variables
    HistVar["truthmass"]["Value"] = Propagator.M()/1000;
    HistVar["met"]["Value"] = p4met.Et()/1000.;
    HistVar["Zpt"]["Value"] = Propagator.Pt()/1000;
    HistVar["ZRap"]["Value"] = Propagator.Rapidity();
    HistVar["njet"]["Value"]=HistVar["njetOR"]["Value"]=HistVar["nbjet"]["Value"]=goodj.size();
    
    if((int)goodj.size()>=2){

        HistVar["jet_eta1"]["Value"] = goodj[0].Eta();
        HistVar["jet_eta2"]["Value"] = goodj[1].Eta();
        HistVar["jet_phi1"]["Value"] = goodj[0].Phi();
        HistVar["jet_phi2"]["Value"] = goodj[1].Phi();
        HistVar["jet_pt1"]["Value"] = goodj[0].Pt();
        HistVar["jet_pt2"]["Value"] = goodj[1].Pt();
        
        TLorentzVector L2j = goodj[0] + goodj[1];
        HistVar["m2j"]["Value"] = L2j.M();
        HistVar["Pt_jj"]["Value"] = L2j.Pt();
        HistVar["Pt_llj1"]["Value"] = (goodj[0] + Propagator).Pt();
        HistVar["Pt_llj2"]["Value"] = (goodj[1] + Propagator).Pt();
        HistVar["Pt_lljj"]["Value"] = (L2j + Propagator).Pt();
        HistVar["etaproduct_jj"]["Value"] = goodj[0].Eta()*goodj[1].Eta();
        HistVar["deltaY_llj1"]["Value"] = fabs(Propagator.Rapidity()-goodj[0].Rapidity());
        HistVar["deltaY_llj2"]["Value"] = fabs(Propagator.Rapidity()-goodj[1].Rapidity());
        HistVar["deltaY_lljj"]["Value"] = fabs(Propagator.Rapidity()-L2j.Rapidity());
        float Rapjj = (goodj[0].Rapidity() + goodj[1].Rapidity())/2;
        float Rapjj_diff = goodj[0].Rapidity() - goodj[1].Rapidity();
        HistVar["centrality"]["Value"] = fabs((Propagator.Rapidity() - Rapjj)/Rapjj_diff);
        HistVar["deltaEtajj"]["Value"] = fabs(goodj[0].Eta() - goodj[1].Eta());
        HistVar["deltaPhijj"]["Value"] = fabs(goodj[0].DeltaPhi(goodj[1]));
        HistVar["deltaRjj"]["Value"] = goodj[0].DeltaR(goodj[1]);
        HistVar["Ht_total"]["Value"] = total_ht;

        deltaeta_jj = fabs(goodj[0].Eta() - goodj[1].Eta());
        deltaR_jj = goodj[0].DeltaR(goodj[1]);
        //Absolute value
        //centrality = fabs((Propagator.Rapidity() - Rapjj)/Rapjj_diff);
        
        centrality = (Propagator.Rapidity() - Rapjj)/Rapjj_diff;

        mass_jj = L2j.M();
        Pt_jj = L2j.Pt();
        dimuon_pt = Propagator.Pt();
        MissingET = p4met.E()*0.001;
        Ht_total = total_ht;

        deltaRap_llj1 = fabs(Propagator.Rapidity()-goodj[0].Rapidity());
        Pt_llj1 = (goodj[0] + Propagator).Pt(); 

        deltaRap_llj2 = fabs(Propagator.Rapidity()-goodj[1].Rapidity());
        Pt_llj2 = (goodj[1] + Propagator).Pt(); 

        deltaRap_lljj = fabs(Propagator.Rapidity()-L2j.Rapidity());
        Pt_lljj = (L2j + Propagator).Pt(); 

        HistVar["bdt_output"]["Value"] = 1; 

        //cout<<"test value: "<<HistVar["bdt_output"]["Value"]<<endl;
  }
  
    CountEvt("NOMINAL", CHN, STEP_cut, FLAG_cut_temp, FLAG_cut, CNT_cut, Evt_Weight); 
    
    //Fill additional histograms
    HistVar["Run"]["Value"] = run;
    HistVar["Event"]["Value"] = event;
    HistVar["PassxAOD"]["Value"] = FLAG_cut["mm"]["xAOD"] ? 1 : 0;    
    HistVar["PassMuonPt"]["Value"] = FLAG_cut["mm"]["MuonPt"] ? 1 : 0;    
    HistVar["PassMuonEta"]["Value"] = FLAG_cut["mm"]["MuonEta"] ? 1 : 0;    
    HistVar["PassLeadPt"]["Value"] = FLAG_cut["mm"]["LeadPt"] ? 1 : 0;    
    HistVar["PassMET"]["Value"] = FLAG_cut["mm"]["MET"] ? 1 : 0;    
    HistVar["MCEventWeight"]["Value"] = 1.;
    HistVar["LeadPt"]["Value"] = leadlep.Pt()/1000;
    HistVar["SubPt"]["Value"] = sublep.Pt()/1000;
    HistVar["LeadEta"]["Value"] = leadlep.Eta();
    HistVar["SubEta"]["Value"] = sublep.Eta();
    HistVar["LeadPhi"]["Value"] = leadlep.Phi();
    HistVar["SubPhi"]["Value"] = sublep.Phi();    
    HistVar["CosThetaStar"]["Value"] = cthstr;
    HistVar["cosTS"]["Value"] = cosTS;
   
    HistVar["deltaEta"]["Value"] = fabs(leadlep.Eta()-sublep.Eta());
    HistVar["deltaPhi"]["Value"] = leadlep.DeltaPhi(sublep);
    HistVar["deltaR"]["Value"] = leadlep.DeltaR(sublep);
    HistVar["ZRap"]["Value"] = Propagator.Rapidity();

    TTree *tree_incl_temp = (TTree*) wk()->getOutputFile ("tree_output")->Get("tree_incl");
    tree_incl_temp->Fill();
    
    FillHistograms("NOMINAL");
  
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: finalize ()
{

  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
/* 
  //<Write ROOT Files>
  TreeFile->Write();
  TreeFile->Close();

  HistoFile->cd();
  HistoFile->Write(0, TObject::kOverwrite);
  HistoFile->Close();
*/

  system("rm -rf cutflow"); 
  system("mkdir -p cutflow"); 
  
  TFile *file3 = wk()->getOutputFile ("cutflow");
  file3->ReOpen("Update");

  for(int i=0; i<(int)CHN.size(); i++) {
    string chn=CHN[i];
    string filename = "log_eff_" + chn + "_physics.txt";
    ofstream file(filename.c_str());

    if(file.is_open()) {
      for(int n=0; n<(int)SYSNAME.size(); n++) {
        string sys = SYSNAME[n];

        if(sys!="NOCORR" && (!SETTING["physics"]["docorr"])) continue;

        if(sys!="NOMINAL" && SETTING["physics"]["docorr"] && (!SETTING["physics"]["dosys"])) continue;

        if(sys=="NOCORR" && SETTING["physics"]["docorr"] && SETTING["physics"]["dosys"]) continue;

        file << "### <Channel : " << chn << "> ; Systematic type is : "
             << sys << " ###" << endl;
        file <<endl;

        for(int j=0; j<(int)STEP_cut.size(); j++) {
          string cut = STEP_cut[j];
          file<<cut<<" = "<<CNT_cut[sys][chn][cut].num<<
          " +/- "<<CNT_cut[sys][chn][cut].err<<endl;
        }
        file << endl;
        file << " ======== border ======== " << endl;
        file << endl;
      }
    }
    else {
      cout<<"Can not open file "<<filename<<endl;
      exit(-1);
    }
    file.close();

    TMacro* m = new TMacro(filename.c_str());
    m->Write();
    delete m;

    string command1 = "mv " + filename + " cutflow";
    system(command1.c_str());
  }
  
  string filename_obj = "log_eff_obj_physics.txt";
  ofstream file_obj(filename_obj.c_str());
  for(int n=0; n<(int)SYSNAME.size(); n++) {
    string sys = SYSNAME[n];

    if(sys!="NOCORR" && (!SETTING["physics"]["docorr"])) continue;

    if(sys!="NOMINAL" && SETTING["physics"]["docorr"] && (!SETTING["physics"]["dosys"])) continue;
        
    if(sys=="NOCORR" && SETTING["physics"]["docorr"] && SETTING["physics"]["dosys"]) continue;

    file_obj << "### <Object Selection> Systematic type is : "
             << sys << " ###" << endl;
    file_obj <<endl;

    MapType_VString::iterator it;
    for(it=STEP_obj.begin(); it!=STEP_obj.end(); it++) {
      string obj=(*it).first;
      for(int i=0; i<(int)STEP_obj[obj].size(); i++) {
        string cut=STEP_obj[obj][i];
        string cutname=obj+"_"+cut;
        file_obj<<cutname<<" = "<<CNT_obj[sys][obj][cut].num
        <<" +/- "<<CNT_obj[sys][obj][cut].err<<endl;
      }
      file_obj<< endl;
    }
    file_obj << endl;
    file_obj << " ======== border ======== " << endl;
    file_obj << endl;

  }
  file_obj.close();


  TMacro* m = new TMacro(filename_obj.c_str());
  m->Write();
  delete m;

  string command2= "mv " + filename_obj + " cutflow";
  system(command2.c_str());



  cout << endl;
  cout << "####" << endl;
  printf("Finalize : %i events have been processed !\n", m_eventCounter);
  printf("Finalize : %i events filtered at truth   !\n", m_filter);
  printf("Finalize : %i events fiducial1 at truth  !\n", m_fidu1);
  printf("Finalize : %i events fiducial2 at truth  !\n", m_fidu2);
  printf("Finalize : %i events fiducial3 at truth  !\n", m_fidu3);
  printf("Finalize MyxAODAnalysis !");
  cout << "####" << endl;
  cout << endl;

  time(&end);
  float timedif = difftime(end,start);
  if(timedif>3600) { printf("Finalize : Time Cost: %f hours\n\n", timedif/3600.); }
  else if(timedif>60) { printf("Finalize : Time Cost: %f minutes\n\n", timedif/60.); }
  else { printf("Finalize : Time Cost: %f second\n\n", timedif); }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histFinalize ()
{


  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
