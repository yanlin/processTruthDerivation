Int_t    run
Int_t    event
Int_t    event_type
Int_t    prod_type

Float_t  m4l_unconstrained
Float_t  mZ1_unconstrained
Float_t  mZ2_unconstrained
Float_t  m4l_fsr
Float_t  mZ1_fsr
Float_t  mZ2_fsr
Float_t  m4l_constrained
Float_t  mZ1_constrained
Float_t  mZ2_constrained 

Float_t  weight_corr
Float_t  weight_lumi
Float_t  weight_sampleoverlap
Float_t  weight

Float_t  Z1_lepplus_pt
Float_t  Z1_lepminus_pt
Float_t  Z2_lepplus_pt
Float_t  Z2_lepminus_pt
Float_t  Z1_lepplus_eta
Float_t  Z1_lepminus_eta
Float_t  Z2_lepplus_eta
Float_t  Z2_lepminus_eta
Float_t  Z1_lepplus_phi
Float_t  Z1_lepminus_phi
Float_t  Z2_lepplus_phi
Float_t  Z2_lepminus_phi
Float_t  Z1_lepplus_m
Float_t  Z1_lepminus_m
Float_t  Z2_lepplus_m
Float_t  Z2_lepminus_m

Int_t    n_jets
Float_t  dijet_invmass
Float_t  dijet_deltaeta
Float_t  leading_jet_pt
Float_t  leading_jet_eta
Float_t  subleading_jet_pt

Float_t  BDT_discriminant
Float_t  BDT_discriminant_VBF
Float_t  BDT_discriminant_HadVH
    
