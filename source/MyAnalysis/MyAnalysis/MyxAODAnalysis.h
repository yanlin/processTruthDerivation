#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <EventLoop/Algorithm.h>
#include "AsgTools/ToolHandle.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "AnalysisVar.h"


#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/SystematicsUtil.h"
#include "PATInterfaces/SystematicVariation.h" 

#include "FsrUtils/FsrPhotonTool.h"

//TMVA Reader 
#include "TMVA/Reader.h"

#include <TTree.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>

#include <TLorentzVector.h>
#include <time.h>
#include <TMacro.h>

#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooPlot.h"
#include "RooGenericPdf.h"
#include "RooMinuit.h"
#include "RooAddPdf.h"

#include "RooRandom.h"

using namespace std;

namespace FSR{
  class FsrPhotonTool;
}

class MyxAODAnalysis : public EL::Algorithm, public AnalysisVar
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  time_t start, end; //!

  xAOD::TEvent *m_event;  //!
  //xAOD::EventInfo* eventInfo; //!
  // count number of events
  bool isMC;
  int m_eventCounter; //!
  Bool_t m_isDerivation; //!
  Bool_t NewFile; //!
  Double_t m_nEventsProcessed; //!
  Double_t m_sumOfWeights; //!
  Double_t m_sumOfWeightsSquared; //!
  Double_t event_mcweight; //!
  Double_t event_totalweight; //!

  int m_filter; //!
  int m_fidu1; //!
  int m_fidu2; //!
  int m_fidu3; //!
  string set;

  string Arg_SampleName; //

  //// tree varaibles
  TTree* tree_incl_4mu; //!
  TTree* tree_incl_4e; //!
  TTree* tree_incl_2mu2e; //!
  TTree* tree_incl_2e2mu; //!

  Float_t deltaeta_jj; //!
  Float_t deltaphi_jj; //!
  Float_t deltaR_jj; //!
  Float_t centrality; //!
  Float_t dimuon_Rap; //!
  Float_t etaproduct_jj; //!
  Float_t mass_jj; //!
  Float_t Pt_jj; //!
  Float_t dimuon_pt; //!
  Float_t MissingET; //!
  Float_t Ht_total; //!
  Float_t deltaRap_llj1; //!
  Float_t Pt_llj1; //!
  Float_t deltaRap_llj2; //!
  Float_t Pt_llj2; //!
  Float_t deltaRap_lljj; //!
  Float_t Pt_lljj; //!

  TH1 *h_truthmass; //!
  TH1 *h_random; //!

  TH3F *h_smear; //!
  TH2F *h_recoeff; //!
  
  TH2F *histo_nmu_pt_eta_truth; //!

  FSR::FsrPhotonTool *fsrTool;//!
  TMVA::Reader *reader; //! 

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  //std::vector<string>* m_setSysList; //!

  // this is a standard constructor
  MyxAODAnalysis ();
  MyxAODAnalysis (string );

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysis, 1);

  void ClearFlags(MapType2_Int& map);
  void ClearWeight(MapType2_Double& map);
  void ClearVariables(MapType2_Int& map);
  void ClearVariables(MapType2_Float& map);
  void ClearVariables(MapType2_Double& map);
  void ClearVariables(MapType2_Double2D& map);
  void ClearVariables(MapType2_VDouble& map);
  void ClearVariables(MapType2_V2DDouble& map);
  void InitStrVec(vector<string>& out, string in, string de=",");
  void InitObjSTEP(MapType_VString& STEP, string obj, string steps);
  void InitSetting(MapType2_Int& setmap, string setname, string settings);
  void CreateCountingMap();

  void InitHistVar(string varlist, int nbin, double xmin, double xmax, string cutstep="");
  void InitHistVar(string varlist, int nbin, double xmin, double xmax, int PDFnum=52, string cutstep="");
  void InitHist2DVar(string varlist, int nxbin, double xmin, double xmax,
                                     int nybin, double ymin, double ymax, string cutstep="");
  void InitHist2DVVar(string varlist, int nxbin, double xmin, double xmax,
                                      int nybin, double ymin, double ymax, string cutstep="");
  void InitTreeVar(string varlist, string type);
  void InitVVar(string varlist, int nbin=0, double xmin=0, double xmax=0, string cutstep="xAOD");
  void AddVarIntoTree(TTree *tree);
  void CreateTreeVarHistoMap(TFile *file);
  void FillHistograms(string sysname);
  double calcCosThetaStar(const TLorentzVector &e, const TLorentzVector &p);


};

#endif
